#代码生成器

###################

#模板说明

常用属性
* @.ClassName 表名/类名
* @.DateTime 时间
* @.Pk 主键
* @.Propertity 表的列
* @.Description 列备注
* @.DataType 列数据类型

---
模板中的关键字

    @(KeyWord)
---
循环代码体

    @.for{
        var temp = @.ClassName.@.Propertity;
    }

![输入图片说明](http://git.oschina.net/uploads/images/2015/0612/145651_29a6a88b_131043.png "在这里输入图片标题")