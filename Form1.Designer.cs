﻿namespace GR_AutoCode_Design
{
    partial class MainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.TbxDBStr = new System.Windows.Forms.TextBox();
            this.BtnLinkDB = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.TbxModel = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.listView2 = new System.Windows.Forms.ListView();
            this.BtnNewPropertity = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.TbxKey = new System.Windows.Forms.TextBox();
            this.TbxValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.TbxPrefix = new System.Windows.Forms.TextBox();
            this.TbxSuffix = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TbxDBStr
            // 
            this.TbxDBStr.Location = new System.Drawing.Point(12, 12);
            this.TbxDBStr.Name = "TbxDBStr";
            this.TbxDBStr.Size = new System.Drawing.Size(258, 21);
            this.TbxDBStr.TabIndex = 0;
            // 
            // BtnLinkDB
            // 
            this.BtnLinkDB.Location = new System.Drawing.Point(276, 11);
            this.BtnLinkDB.Name = "BtnLinkDB";
            this.BtnLinkDB.Size = new System.Drawing.Size(172, 23);
            this.BtnLinkDB.TabIndex = 1;
            this.BtnLinkDB.Text = "连接";
            this.BtnLinkDB.UseVisualStyleBackColor = true;
            this.BtnLinkDB.Click += new System.EventHandler(this.BtnLinkDB_Click);
            // 
            // listView1
            // 
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 68);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(258, 364);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(476, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "生成";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(476, 37);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(161, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "模板设计器";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // TbxModel
            // 
            this.TbxModel.Location = new System.Drawing.Point(12, 39);
            this.TbxModel.Name = "TbxModel";
            this.TbxModel.ReadOnly = true;
            this.TbxModel.Size = new System.Drawing.Size(258, 21);
            this.TbxModel.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(276, 37);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(172, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "选择模板";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // listView2
            // 
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.HideSelection = false;
            this.listView2.LabelEdit = true;
            this.listView2.Location = new System.Drawing.Point(276, 69);
            this.listView2.MultiSelect = false;
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(363, 239);
            this.listView2.TabIndex = 4;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // BtnNewPropertity
            // 
            this.BtnNewPropertity.Location = new System.Drawing.Point(476, 314);
            this.BtnNewPropertity.Name = "BtnNewPropertity";
            this.BtnNewPropertity.Size = new System.Drawing.Size(163, 23);
            this.BtnNewPropertity.TabIndex = 3;
            this.BtnNewPropertity.Text = "添加新属性";
            this.BtnNewPropertity.UseVisualStyleBackColor = true;
            this.BtnNewPropertity.Click += new System.EventHandler(this.BtnNewPropertity_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(277, 314);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(173, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "删除";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // TbxKey
            // 
            this.TbxKey.Location = new System.Drawing.Point(328, 343);
            this.TbxKey.Name = "TbxKey";
            this.TbxKey.Size = new System.Drawing.Size(128, 21);
            this.TbxKey.TabIndex = 5;
            // 
            // TbxValue
            // 
            this.TbxValue.Location = new System.Drawing.Point(329, 370);
            this.TbxValue.Name = "TbxValue";
            this.TbxValue.Size = new System.Drawing.Size(128, 21);
            this.TbxValue.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(281, 346);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "Key";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 373);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "Value";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(278, 397);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(361, 34);
            this.button5.TabIndex = 3;
            this.button5.Text = "保存";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // TbxPrefix
            // 
            this.TbxPrefix.Location = new System.Drawing.Point(511, 343);
            this.TbxPrefix.Name = "TbxPrefix";
            this.TbxPrefix.Size = new System.Drawing.Size(128, 21);
            this.TbxPrefix.TabIndex = 5;
            // 
            // TbxSuffix
            // 
            this.TbxSuffix.Location = new System.Drawing.Point(511, 370);
            this.TbxSuffix.Name = "TbxSuffix";
            this.TbxSuffix.Size = new System.Drawing.Size(128, 21);
            this.TbxSuffix.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(463, 346);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "Prefix";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(463, 373);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "Suffix";
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 444);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbxSuffix);
            this.Controls.Add(this.TbxValue);
            this.Controls.Add(this.TbxPrefix);
            this.Controls.Add(this.TbxKey);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.BtnNewPropertity);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.BtnLinkDB);
            this.Controls.Add(this.TbxModel);
            this.Controls.Add(this.TbxDBStr);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(565, 482);
            this.Name = "MainFrm";
            this.Text = "AutuCode";
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TbxDBStr;
        private System.Windows.Forms.Button BtnLinkDB;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox TbxModel;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.Button BtnNewPropertity;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox TbxKey;
        private System.Windows.Forms.TextBox TbxValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox TbxPrefix;
        private System.Windows.Forms.TextBox TbxSuffix;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

